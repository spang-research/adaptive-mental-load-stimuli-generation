#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from DifficultyEstimation.difficulty_estimation_system import DifficultyEstimationSystem
from DifficultyEstimation.task_generator import TaskGenerator
from termios import tcflush, TCIFLUSH
from sklearn.utils import shuffle
import pandas
import signal
import sys
import time



class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



class TestGame:

    def __init__(self, difficulty = None):
        self.generate_tasks = True

        # prepare taking tasks from a predefined list
        if difficulty != None:
            print("Start a %s-difficulty session:", difficulty)
            self.generate_tasks = False
            tasks = pandas.read_csv("all_tasks_difficulty_class.csv", usecols = range(1, 7))
            self.tasks = shuffle(tasks[tasks.difficulty_class == difficulty])
            self.current_index = -1

            if difficulty == "low":
                self.time_bonus_factor = 2
            elif difficulty == "medium":
                self.time_bonus_factor = 1.3
            elif difficulty == "high":
                self.time_bonus_factor = 1

        signal.signal(signal.SIGALRM, self.interrupted)
        self.estimation_system = DifficultyEstimationSystem()
        self.responses = []
        self.input_text()

    def get_next_task(self):
        if self.generate_tasks:
            return self.estimation_system.generate_task_with_difficulty()

        else:
            self.current_index += 1
            object = self.tasks.iloc[self.current_index]
            estimation = self.estimation_system.estimate_time_needed_for_task(object.x, object.y, object.operation)
            (time_for_task, individual_factor, time_offset) = estimation

            # add time plultiplier based on task category
            time_for_task = time_for_task * self.time_bonus_factor
            return (object.x, object.y, object.operation, object.result, time_for_task, individual_factor, time_offset)

    def interrupted(self, signum, frame):
        raise TimeoutError("Timeout!")

    def input_text(self):
        signal.alarm(0)
        time.sleep(2)

        # generate new task
        (x, y, operation, result, time_for_task, performance_index, time_bonus) = self.get_next_task()

        # print task
        operators = {'plus': '+', 'minus': '-', 'mal': '*', 'durch': '/'}
        localized_operation = TaskGenerator.translate_operation_to_german(operation)
        print("\n%s%s %s %s%s" % (bcolors.BOLD + bcolors.FAIL, x, operators[localized_operation], y, bcolors.ENDC))
        print("you got %s seconds!" % round(time_for_task, 1))
        # print("(%s, pidx %s, time offset %ss)" % (len(self.responses), performance_index, time_bonus))

        # start timer
        signal.setitimer(signal.ITIMER_REAL, time_for_task)
        start_time = time.time()

        try:
            tcflush(sys.stdin, TCIFLUSH)
            response = input("result: ")
            end_time = time.time()
            response_time = round(end_time - start_time, 5)

            try:
                response = int(response)
            except ValueError:
                response = -1

            print("reaction time %s, time left %s" % (round(response_time, 2), round((time_for_task - response_time), 2)))
            correct_response = response == result
            print("that's correct!\n" if correct_response else "that's wrong, the corret answer is %s\n" % result)
            self.estimation_system.add_response(correct_response, response_time, True, operation)
            self.estimation_system.print_current_stats()
            self.responses.append({
                'x': x,
                'operation': "\"%s\"" % operation,
                'y': y,
                'result': result,
                'correct_response': 1 if correct_response else 0,
                'response_time': response_time,
                'time_for_task': time_for_task,
                'performance_index': performance_index,
                'time_bonus': time_bonus
            })

            # restart
            self.input_text()

        except TimeoutError:
            print("\nTime's up! (result: %s)\n" % result)
            self.estimation_system.add_response(False, 0, False, operation)
            self.estimation_system.print_current_stats()
            self.responses.append({
                'x': x,
                'operation': "\"%s\"" % operation,
                'y': y,
                'result': result,
                'correct_response': 0,
                'response_time': 'NA',
                'time_for_task': time_for_task,
                'performance_index': performance_index,
                'time_bonus': time_bonus,
            })

            # restart
            self.input_text()

        except KeyboardInterrupt:
            signal.alarm(0)

            percentage_correct = (sum(map(lambda x: x['correct_response'], self.responses)) / len(self.responses)) * 100.0
            print("\n\nThe experiment is over!")
            print("%s%s%% correct answers!%s" % (bcolors.BOLD + bcolors.OKBLUE, round(percentage_correct, 2), bcolors.ENDC))
            print("Thank you very much!")

            print("\n" + bcolors.BOLD + bcolors.OKBLUE + "Results:" + bcolors.ENDC)
            for element in self.responses:
                print("%s, %s, %s, %s, %s, %s, %s, %s, %s" % (element['x'], element['operation'], element['y'], element['result'], element['correct_response'], element['response_time'], element['time_for_task'], element['performance_index'], element['time_bonus']))



if __name__ == "__main__":
    # game = TestGame("low")
    # game = TestGame("medium")
    # game = TestGame("high")
    game = TestGame()
