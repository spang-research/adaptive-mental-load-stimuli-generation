#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import python libs

from flask import Flask, request, redirect, url_for
from flask import jsonify
from os.path import isfile, join
import argparse
import datetime
import json
import os
import sys
import random
import string
import threading
import requests

# import project libs

from DifficultyEstimation.difficulty_estimation_system import DifficultyEstimationSystem

# constants and globals

global estimation_system

# flask methods

app = Flask(__name__)

@app.route('/next_task', methods=['POST'])
def next_task():

    # get or generate a new session ID
    if 'session_id' in request.values:
        session_id = request.values['session_id']

        # save response details if present
        a = 'correct_response' in request.values
        b = 'response_time' in request.values
        c = 'in_time' in request.values
        d = 'operation' in request.values
        if a and b and c and d:
            try:
                estimation_system.add_response(
                    bool(int(request.values['correct_response'])),
                    float(request.values['response_time']),
                    bool(int(request.values['in_time'])),
                    request.values['operation'],
                    session_id,
                    write_response_to_log = True
                )
            except:
                return create_json_response_from({
                    'error': 'malformed response values'
                }, 400)

    else:
        session_id = randomString(24)

    print("next_task request for session ID %s" % session_id)
    task = estimation_system.generate_task_with_difficulty(session_id, write_response_to_log = True)
    (x, y, operation, result, estimated_response_time_rounded, individual_factor, time_offset) = task

    (
        overall_score,
        correctness,
        performance_index,
        overall_time_offset,
        in_time_responses,
        overall_time_left,
        task_difficulty_level
    ) = estimation_system.overall_adaptation_stats(session_id)

    return create_json_response_from({
        'first_number': int(x),
        'second_number': int(y),
        'operation': operation,
        'result': int(result),
        'difficulty': estimated_response_time_rounded,
        'individual_factor': individual_factor,
        'time_offset': time_offset,
        'overall_score': overall_score,
        'correctness': correctness,
        'performance_index': performance_index,
        'overall_time_offset': float(overall_time_offset),
        'in_time_responses': in_time_responses,
        'average_time_left': overall_time_left,
        'task_difficulty_level': float(task_difficulty_level),
        'session_id': session_id
    }, 202)

@app.route('/cleanup', methods=['GET'])
def cleanup():
    sessions = estimation_system.garbage_collect_session_list()
    return create_json_response_from(
        {'server': "removed %s old session keys" % sessions},
        200
    )

# helper

def create_json_response_from(hash, code):
    response = jsonify(hash)
    response.status_code = code

    print('respond with:', response)
    sys.stdout.flush()
    return response

def randomString(stringLength = 24):
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for i in range(stringLength))



# entry point as a stand alone script
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'Mental Calculation Difficulty Estimation'
    )

    args = parser.parse_args()

    # create instances
    global estimation_system
    estimation_system = DifficultyEstimationSystem()

    # start flask http server
    app.run(host = '0.0.0.0')
