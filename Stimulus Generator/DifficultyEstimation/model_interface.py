#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# from DifficultyEstimation.StepEstimation.TaskFeaturesCache import TaskFeaturesCache
from sklearn import preprocessing
import tensorflow as tf
import numpy as np
import pickle
import os



class ModelInterface:

    def __init__(self):
        # load the tensorflow / keras NN for the time estimation
        addition_model_path = os.path.join(os.path.dirname(__file__), 'model/tf_neural_network_model_individual_addition.h5')
        self.addition_estimator = tf.keras.models.load_model(addition_model_path)

        addition_scaler_path = os.path.join(os.path.dirname(__file__), 'model/model_addition_input_scaler.pickle')
        self.addition_scaler = pickle.load(open(addition_scaler_path, 'rb'))

        subtraction_model_path = os.path.join(os.path.dirname(__file__), 'model/tf_neural_network_model_individual_subtraction.h5')
        self.subtraction_estimator = tf.keras.models.load_model(subtraction_model_path)

        subtraction_scaler_path = os.path.join(os.path.dirname(__file__), 'model/model_subtraction_input_scaler.pickle')
        self.subtraction_scaler = pickle.load(open(subtraction_scaler_path, 'rb'))

        multiplication_model_path = os.path.join(os.path.dirname(__file__), 'model/tf_neural_network_model_individual_multiplication.h5')
        self.multiplication_estimator = tf.keras.models.load_model(multiplication_model_path)

        multiplication_scaler_path = os.path.join(os.path.dirname(__file__), 'model/model_multiplication_input_scaler.pickle')
        self.multiplication_scaler = pickle.load(open(multiplication_scaler_path, 'rb'))

        division_model_path = os.path.join(os.path.dirname(__file__), 'model/tf_neural_network_model_individual_division.h5')
        self.division_estimator = tf.keras.models.load_model(division_model_path)

        division_scaler_path = os.path.join(os.path.dirname(__file__), 'model/model_division_input_scaler.pickle')
        self.division_scaler = pickle.load(open(division_scaler_path, 'rb'))

        # # load the task features cache
        # self.tfc = TaskFeaturesCache()

        # minimum time per task in seconds
        self.min_time_per_task = 0.5

    def predict_difficulty_for(self, x, y, operation, individual_factor = 0.0):
        # x = 10
        # y = 13
        # if operation == "addition":
        #     step_features = self.tfc.features_for(x, y)
        #     step_features.update_features_for_performance_index(individual_factor)
        #     step_types = step_features.estimated_step_types
        #
        #     features = [[
        #         step_features.carryover_count,
        #         individual_factor,
        #         step_types['count'],
        #         step_types['carryover'],
        #         step_types['demask'],
        #         step_types['trivial_demask'],
        #         step_types['find_compensation'],
        #         step_types['mask'],
        #         step_types['trivial_mask'],
        #         step_types['memorize'],
        #         step_types['partitioning_for_bridging'],
        #         step_types['recall'],
        #         step_types['remember'],
        #         step_types['reorder'],
        #         step_types['split_part'],
        #         step_types['part_split'],
        #         step_types['seq_split'],
        #         step_types['trivial_recall'],
        #         step_types['concatenate'],
        #         step_features.variations,
        #         step_features.max_step_count,
        #         step_features.unique_step_count,
        #         step_features.estimated_step_count,
        #         step_features.estimated_weighted_steps,
        #         step_features.range_median_step_count,
        #         step_features.range_mean_step_count,
        #         step_features.range_median_step_weight,
        #         step_features.range_mean_step_weight,
        #         step_features.total_mean_step_weight,
        #         step_features.total_mean_step_count,
        #         step_features.total_median_step_count,
        #         step_features.total_median_step_weight
        #     ]]
        #
        # else:
        first_minor = x % 10
        second_minor = y % 10
        result = 0
        carryover = 0
        first_number_even = 1 if first_minor % 2 == 0 else 0
        second_number_even = 1 if second_minor % 2 == 0 else 0
        first_number_zero = 1 if first_minor == 0 else 0
        second_number_zero = 1 if second_minor == 0 else 0

        if operation == "subtraction":
            result = x - y
            carryover = self.carryover_subtraction(x, y)

        elif operation == "multiplication":
            result = x * y
            carryover = 1 if first_minor > 6 or second_minor > 6 else 0

        elif operation == "division":
            result = x / y
            carryover = 1 if result > 10 else 0

        else: # addition
            result = x + y
            carryover = self.carryover_addition(x, y)

        result_even = 1 if result % 2 == 0 else 0
        result_zero = 1 if result % 10 == 0 else 0
        result_minor = result % 10

        features = np.array([[
            x,
            y,
            result,
            first_minor,
            second_minor,
            result_minor,
            carryover,
            first_number_even,
            second_number_even,
            result_even,
            first_number_zero,
            second_number_zero,
            result_zero,
            individual_factor
        ]])

        estimation = self.select_model_predict(operation, features)
        estimation = max(self.min_time_per_task, estimation)
        return estimation

    def select_model_predict(self, operation, feature_list):
        if operation == 'addition':
            scaled_features = self.addition_scaler.transform(feature_list)
            return self.addition_estimator.predict(scaled_features)[0][0]

        elif operation == 'subtraction':
            scaled_features = self.subtraction_scaler.transform(feature_list)
            return self.subtraction_estimator.predict(scaled_features)[0][0]

        elif operation == 'multiplication':
            scaled_features = self.multiplication_scaler.transform(feature_list)
            return self.multiplication_estimator.predict(scaled_features)[0][0]

        scaled_features = self.division_scaler.transform(feature_list)
        return self.division_estimator.predict(scaled_features)[0][0]


    def carryover_addition(self, first_number, second_number):
        carryover_count = 0
        carryover_last_iteration = 0
        index = 0
        length_min_number = len(str(min(first_number, second_number)))
        while index < length_min_number or carryover_last_iteration == 1:
            modulo_number = pow(10, index + 1)
            int_divisor = pow(10, index)
            first_digit = (first_number % modulo_number) // int_divisor
            second_digit = (second_number % modulo_number) // int_divisor
            if first_digit + second_digit + carryover_last_iteration >= 10:
                carryover_count += 1
                carryover_last_iteration = 1
            else:
                carryover_last_iteration = 0
            index += 1
        return carryover_count

    def carryover_subtraction(self, first_number, second_number):
        carryover_count = 0
        carryover_last_iteration = 0
        index = 0
        length_min_number = len(str(min(first_number, second_number)))
        while index < length_min_number or carryover_last_iteration == 1:
            modulo_number = pow(10, index + 1)
            int_divisor = pow(10, index)
            first_digit = (first_number % modulo_number) // int_divisor
            second_digit = (second_number % modulo_number) // int_divisor
            if first_digit - second_digit - carryover_last_iteration < 0:
                carryover_count += 1
                carryover_last_iteration = 1
            else:
                carryover_last_iteration = 0
            index += 1
        return carryover_count

if __name__ == "__main__":
    estimator = ModelInterface()
    seconds = estimator.predict_response_time_for(27, 7, 'addition')
    print("\nModel Interface Unit")
    print("testrun: for the mental calculation of 27 + 7 we estimate to need %ss" % seconds)
