#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .value_store_per_operation import ValueStorePerOperation
import numpy as np



# Lower end of the range, mean value and upper end of the range
# of time needed per operation type.
# Used to estimate the individual performance per operation
OPERATION_PERFORMANCE_BOUNDS = {
    'addition': [-2.5857642446411773, 5.433844836312346, 6.268791191978368],
    'subtraction': [-1.967790890089236, 4.9498692497145385, 5.746889962110435],
    'multiplication': [-2.3681554586626596, 5.248698281357066, 7.416790646747901],
    'division': [-1.8560805896588133, 4.899876780807972, 6.213613445953522]
}



class Adaptation:

    def __init__(self):
        # response time cache for individual performance index
        self.individual_difficulty_cache = ValueStorePerOperation()

        # responded in time cache for time offset
        self.last_responses_in_time = ValueStorePerOperation()

        # save the correctness of the responses for analysis purposes
        self.correct_responses_cache = ValueStorePerOperation()

        # save the amount of time left for each trial and operation
        self.time_left_cache = ValueStorePerOperation()

        # time offset for each of the four operations
        self.time_offsets = {
            'addition': 1.0,
            'subtraction': 1.0,
            'multiplication': 1.0,
            'division': 1.0
        }

        self.task_difficulty_cache = [0.0]

        # individual performance adaptation parameters
        self.sliding_window_size = 20 # number of elements in cache lists
        self.target_timeout_rate_upper_bound = 0.85 # [0, 1], target timeout rate is 20%, +- 5%
        self.target_timeout_rate_lower_bound = 0.75 # [0, 1]
        self.target_timeout_update_rate = 0.2 # seconds, correction rate of the time offset
        self.max_absolute_time_offset = 10.0 # seconds, maximum time manipulation
        self.offset_miss_factor = 0.015

        # last task stats
        self.time_given_for_last_task = 0.0
        self.reaction_time_for_last_task = 0.0
        self.last_individual_factor = 0.0



    # public API

    def compute_inidividual_factor_for(self, operation):
        print("compute_inidividual_factor_for operation")
        # compute individual performance based on last responses made in time
        performance_values = self.individual_difficulty_cache.recent_values(operation)
        hit_miss_values = self.last_responses_in_time.recent_values(operation)

        if len(performance_values) >= 2:
            return self.compute_inidividual_factor(performance_values, hit_miss_values, operation)

        return 0.0

    def compute_time_offset(self, operation):
        # compute time offset based on last responses
        if self.last_responses_in_time.count(operation) >= 2:
            timeout_ratio = self.last_responses_in_time.weighted_mean_recent_values(operation)

            # adjust time offset if ratio is out of bounds
            if timeout_ratio > self.target_timeout_rate_upper_bound:
                self.time_offsets[operation] -= self.target_timeout_update_rate
            else:
                last_response_in_time = self.last_responses_in_time.last(operation)
                if not last_response_in_time and timeout_ratio < self.target_timeout_rate_lower_bound:
                    self.time_offsets[operation] += self.target_timeout_update_rate

            # cap value
            if abs(self.time_offsets[operation]) > self.max_absolute_time_offset:
                self.time_offsets[operation] = self.max_absolute_time_offset * (self.time_offsets[operation] / abs(self.time_offsets[operation]))

            return self.time_offsets[operation]

        return 0.0

    def compute_task_difficulty_level(self):
        # compute time offset based on last responses
        # last_responses = [] # TODO: create list of samples taken from the current difficulty class

        if self.last_responses_in_time.total_count() >= 2:
            grand_timeout_ratio = self.last_responses_in_time.grand_weighted_mean_recent_values()

            # adjust time offset if ratio is out of bounds
            current_difficulty = self.current_task_difficulty()
            new_difficulty_level = current_difficulty
            if grand_timeout_ratio > self.target_timeout_rate_upper_bound and current_difficulty < 1.0:
                new_difficulty_level = round(current_difficulty + 0.1, 1)
            elif grand_timeout_ratio < self.target_timeout_rate_lower_bound and current_difficulty > 0.0:
                new_difficulty_level = round(current_difficulty - 0.1)

            self.task_difficulty_cache.append(new_difficulty_level)

    def add_response(self, correct_response, reaction_time, in_time, operation):
        if in_time:
            self.reaction_time_for_last_task = reaction_time
            time_left = self.time_given_for_last_task - reaction_time
            self.time_left_cache.add(operation, time_left)
        else:
            self.reaction_time_for_last_task = 0.0

        # add in time values to lists & resize lists if necessary
        self.last_responses_in_time.add(operation, in_time)

        # save the correct response value
        self.correct_responses_cache.add(operation, correct_response)

        # add individual time needed to cache-lists, but only if the response was made in time
        if in_time:
            individual_difficulty = reaction_time
            if not correct_response:
                individual_difficulty += 2

                global_mean_performance = OPERATION_PERFORMANCE_BOUNDS[operation][1]
                upper_bound = OPERATION_PERFORMANCE_BOUNDS[operation][2]
                max_time = global_mean_performance + upper_bound

                if individual_difficulty > max_time:
                    individual_difficulty = max_time
        else:
            individual_difficulty = self.time_given_for_last_task + 2

        self.individual_difficulty_cache.add(operation, individual_difficulty)
        self.individual_difficulty_cache.print()
        self.last_responses_in_time.print()

        # compute a new task difficulty level
        self.compute_task_difficulty_level()

    def current_task_difficulty(self):
        return self.task_difficulty_cache[-1]

    # stats

    def stats_for(self, operation):
        element_count = self.last_responses_in_time.count(operation)

        # adaptation metrics
        current_individual_factor = round(self.compute_inidividual_factor_for(operation), 2)
        time_offset = round(self.time_offsets[operation], 2)

        in_time = self.last_responses_in_time.mean_recent_values(operation)
        in_time = round(in_time * 100.0, 2)

        # last task stats
        rt = round(self.reaction_time_for_last_task, 2)
        if rt > 0:
            time_left = self.time_given_for_last_task - self.reaction_time_for_last_task
            time_left = round(time_left, 2)
        else:
            time_left = 0.0

        return (element_count, current_individual_factor, time_offset, in_time, rt, time_left)

    def overall_stats(self):
        overall_correctness = self.correct_responses_cache.grand_mean()
        overall_in_time_responses = self.last_responses_in_time.grand_mean()
        overall_time_left = self.time_left_cache.grand_mean()
        overall_performance_index = self.compute_overall_performance_index()
        overall_time_offset = self.compute_overall_time_offset()

        overall_score = (overall_correctness + (overall_performance_index * -0.2)) * 100.0
        overall_score = min(overall_score, 100.0)
        overall_score = max(overall_score, 0.0)

        return (
            round(overall_score, 2),
            round(overall_correctness * 100.0, 2),
            round(overall_performance_index, 2),
            round(overall_time_offset, 2),
            round(overall_in_time_responses * 100.0, 2),
            round(overall_time_left, 2),
            self.current_task_difficulty()
        )

    # internal API

    def compute_inidividual_factor(self, performance_values, hit_miss_values, operation):
        if len(performance_values) < 1:
            return 0.0

        print(performance_values)
        print(hit_miss_values)

        np_performance = np.array(performance_values)
        correct_response_perfoemance_values = np_performance[hit_miss_values]
        individual_difficulty = np.mean(correct_response_perfoemance_values)

        # compute & scale the delte between actual and reference difficulty
        delta = individual_difficulty - OPERATION_PERFORMANCE_BOUNDS[operation][1]

        # ratio between current performance and known population
        if delta > 0:
            ratio = delta / OPERATION_PERFORMANCE_BOUNDS[operation][2]
        else:
            ratio = (delta / OPERATION_PERFORMANCE_BOUNDS[operation][0]) * -1

        # add offset for misses
        normed_ratio = self.norm_performance_range(ratio)
        range_to_max = 1 - normed_ratio
        misses = sum(map(lambda x: not x, hit_miss_values))
        offset = misses * (range_to_max / len(hit_miss_values)) # dynamic factor
        individual_factor = self.norm_performance_range(normed_ratio + offset)
        self.last_individual_factor = individual_factor

        return individual_factor

    def norm_performance_range(self, value):
        # normalise delta inbetween [-1, 1]
        step_1 = min(1.0, value)
        return max(-1.0, step_1)

    def compute_overall_performance_index(self):
        print("compute_overall_performance_index")
        performances = []

        for op in ['addition', 'subtraction', 'multiplication', 'division']:
            performance_values = self.individual_difficulty_cache.all_values_of_type(op)
            hit_miss_values = self.last_responses_in_time.all_values_of_type(op)

            if len(performance_values) >= 2:
                performance_index = self.compute_inidividual_factor(performance_values, hit_miss_values, op)
                performances.append(performance_index)

        if len(performances) > 0:
            return np.array(performances).mean()

        return 0.0

    def compute_overall_time_offset(self):
        offsets = []

        for op in ['addition', 'subtraction', 'multiplication', 'division']:
            offsets.append(self.time_offsets[op])

        if len(offsets) > 0:
            return np.array(offsets).mean()

        return 0.0



if __name__ == "__main__":
    print("Adaptation Unit")
