#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np



class ValueStorePerOperation:

    def __init__(self, sliding_window_size = 20):
        self.sliding_window_size = sliding_window_size
        self.store = {
            'addition': [],
            'subtraction': [],
            'multiplication': [],
            'division': []
        }

    def add(self, operation, value):
        self.store[operation].append(value)

    def recent_values(self, operation):
        count = min(len(self.store[operation]), self.sliding_window_size)
        return self.store[operation][-count:]

    def all_values_of_type(self, operation):
        return self.store[operation]

    def all_values(self):
        values = self.store['addition'].copy()
        values.extend(self.store['subtraction'])
        values.extend(self.store['multiplication'])
        values.extend(self.store['division'])
        return values

    def mean_recent_values(self, operation):
        values = self.recent_values(operation)
        if len(values) > 0:
            return np.array(values).mean()

        return 0.0

    def weighted_mean_recent_values(self, operation):
        values = self.recent_values(operation)
        count = len(values)

        if count > 0:
            weights = np.array(range(24, 101, 4)) / 100
            relevant_weights = weights[-count:]

            weighted_history = values * relevant_weights
            return np.mean(weighted_history) * (1 / np.mean(relevant_weights))

        return 0.0

    def grand_weighted_mean_recent_values(self):
        mean_values = []
        for op in ['addition', 'subtraction', 'multiplication', 'division']:
            if len(self.store[op]) > 0:
                mean_values.append(self.weighted_mean_recent_values(op))

        if len(mean_values) > 0:
            return np.array(mean_values).mean()

        return 0.0

    def grand_mean(self):
        values = self.all_values()
        if len(values) > 0:
            return np.array(values).mean()

        return 0.0

    def count(self, operation):
        return len(self.store[operation])

    def total_count(self):
        return len(self.all_values())

    def last(self, operation):
        return self.store[operation][-1]

    def print(self):
        print(self.store)
