#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scipy.stats import gamma
import numpy as np
import pandas as pd
import random
import os



class TaskGenerator:

    def __init__(self):
        # create a sampling distribution (gamma) for task generation (multiplication & division)
        data_gamma = gamma.rvs(a = 6, scale = 1, size = 10000)
        self.sampling_distribution = (data_gamma / max(data_gamma)) * 66

        # read referece task list
        file_path = os.path.join(os.path.dirname(__file__), 'reference_data/all_tasks.csv')
        self.reference_task_list = pd.read_csv(file_path)

    def generate_task(self):
        operation = random.choice(["addition", "subtraction", "multiplication", "division"])
        # operation = np.random.choice(["addition", "subtraction", "multiplication", "division"], p = [0.134, 0.132, 0.134, 0.6])
        # operation = "addition"

        def generate_multiplication():
            first_number = int(np.random.choice(self.sampling_distribution))
            if first_number == 1: first_number = 2
            max_multiplicator = int(200 / first_number)
            second_number = random.randint(2, max_multiplicator)
            result = first_number * second_number
            return (first_number, second_number, result)

        if operation == "multiplication":
            (first_number, second_number, result) = generate_multiplication()

        elif operation == "division":
            numbers = generate_multiplication()
            dividend_index = random.choice([0, 1])
            first_number = numbers[2]
            second_number = numbers[dividend_index]
            result = numbers[1 - dividend_index]

        elif operation == "subtraction":
            first_number = random.randint(2, 200)
            max_subtrahend = first_number - 1
            second_number = random.randint(1, max_subtrahend)
            result = first_number - second_number

        else: # addition
            result = random.randint(2, 200)
            first_number = random.randint(1, result - 1)
            second_number = result - first_number

        return (first_number, second_number, operation, result)

    def select_task_with_difficulty(self, target_difficulty):
        operation = random.choice(["addition", "subtraction", "multiplication", "division"])
        filter_mask = (self.reference_task_list.difficulty_class == target_difficulty) & (self.reference_task_list.operation == operation)
        selected_sample = None

        try:
            selected_sample = self.reference_task_list[filter_mask].sample()
        except ValueError:
            selected_sample = self.reference_task_list.sample()

        return (selected_sample.x.values[0],
                selected_sample.y.values[0],
                selected_sample.operation.values[0],
                selected_sample.result.values[0])

    @staticmethod
    def translate_operation_to_german(operation):
        if operation == "multiplication":
            return "mal"
        elif operation == "division":
            return "durch"
        elif operation == "subtraction":
            return "minus"
        else: # addition
            return "plus"



if __name__ == "__main__":
    generator = TaskGenerator()
    print("Testrun:\n%s %s %s = %s" % generator.generate_task())
