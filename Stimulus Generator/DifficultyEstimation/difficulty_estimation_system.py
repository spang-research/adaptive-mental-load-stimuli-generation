#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# import python libs

from datetime import datetime, timedelta

# import project libs

from .model_interface import ModelInterface
from .adaptation import Adaptation
from .task_generator import TaskGenerator



class DifficultyEstimationSystem:

    def __init__(self):
        # create model and task generator instances
        self.estimator = ModelInterface()
        self.task_generator = TaskGenerator()

        # initialise session cache
        self.sessions = []

        # log file
        self.log_filename = "app log/task_log.csv"

    # public API

    def generate_task_with_difficulty(self, session_id = 'default', write_response_to_log = False):
        # get session index for given session ID
        index = self.session_index_for(session_id)

        # generate new task
        (x, y, operation, result) = self.generate_task(index = index)

        # update last operation attribute in sessions list
        self.sessions[index]['last_operation'] = operation

        # get difficulty estimation and used adaptation values
        estimation = self.estimate_time_needed_for_task(x, y, operation, index = index)
        (difficulty, individual_factor, time_offset) = estimation

        # save task deatails to log file
        if write_response_to_log:
            self.write_log(session_id, x = x, operation = operation, y = y, result = result, estimated_difficulty = difficulty, individual_factor = individual_factor, time_offset = time_offset)

        # return task & difficulty combination
        return (x, y, operation, result, difficulty, individual_factor, time_offset)

    def generate_task(self, session_id = 'default', index = -1):
        target_difficulty = self.adaptivity(index).current_task_difficulty()
        (first_number, second_number, operation, result) = self.task_generator.select_task_with_difficulty(target_difficulty)
        # (first_number, second_number, operation, result) = self.task_generator.generate_task()

        # update last operation attribute in sessions list & return task
        if index < 0:
            index = self.session_index_for(session_id)

        self.sessions[index]['last_operation'] = operation
        self.sessions[index]['task_count'] += 1
        return (first_number, second_number, operation, result)

    def estimate_time_needed_for_task(self, x, y, operation, session_id = 'default', index = -1):
        # get corresponding adaptation instance from session list
        if index < 0:
            index = self.session_index_for(session_id)
        adaptation_instance = self.adaptivity(index)

        # compute current adaptation parameters
        individual_factor = adaptation_instance.compute_inidividual_factor_for(operation)
        time_offset = adaptation_instance.compute_time_offset(operation)

        # predict difficulty with individual performance value & add time offset
        estimated_difficulty = self.estimator.predict_difficulty_for(x, y, operation, individual_factor)
        estimated_difficulty_rounded = round(estimated_difficulty + time_offset, 2)
        self.adaptivity(index).time_given_for_last_task = estimated_difficulty_rounded

        # return a rounded difficulty in seconds
        return (estimated_difficulty_rounded, round(individual_factor, 2), round(time_offset, 2))

    def add_response(self, correct_response, response_time, in_time, operation = False, session_id = 'default', write_response_to_log = False):
        index = self.session_index_for(session_id)

        if operation == False:
            operation = self.sessions[index]['last_operation']

        self.adaptivity(index).add_response(correct_response, response_time, in_time, operation)

        # save task details to log file
        if write_response_to_log:
            self.write_log(session_id, correct_response = correct_response, response_time = response_time, in_time = in_time, operation = operation)

    def adaptation_stats(self, session_id = 'default'):
        index = self.session_index_for(session_id)
        last_operation = self.sessions[index]['last_operation']
        return self.adaptivity(index).stats_for(last_operation)

    def overall_adaptation_stats(self, session_id = 'default'):
        index = self.session_index_for(session_id)
        return self.adaptivity(index).overall_stats()

    def print_current_stats(self, session_id = 'default'):
        index = self.session_index_for(session_id)
        session = self.sessions[index]
        adaptation_instance = session['adaptivity_instance']

        (
            element_count,
            current_performance_index,
            time_offset,
            in_time,
            reaction_time_for_last_task,
            time_left
        ) = adaptation_instance.stats_for(session['last_operation'])

        (
            overall_score,
            correctness,
            performance_index,
            overall_time_offset,
            in_time_responses,
            overall_time_left,
            task_difficulty_level
        ) = adaptation_instance.overall_stats()

        print("\n#%s - %ss reaction time, %ss left" % (session['task_count'], reaction_time_for_last_task, time_left))
        print("current: %s performance, %ss time offset, %s%% in time (last %s %s results)" % (current_performance_index, time_offset, in_time, element_count, session['last_operation']))
        print("overall: %s score, level %s, %s performance, %ss offset, %s%% correctness, %s%% in time, avg. %ss left" % (overall_score, task_difficulty_level, performance_index, overall_time_offset, correctness, in_time_responses, overall_time_left))

    # internal API

    def session_index_for(self, session_id):
        results = [idx for idx, element in enumerate(self.sessions) if element['session_id'] == session_id]
        if len(results) > 0:
            return results[0]

        self.register_new_session_id(session_id)
        return len(self.sessions) - 1

    def adaptivity(self, index):
        return self.sessions[index]['adaptivity_instance']

    def register_new_session_id(self, session_id):
        print("register new session ID '%s'" % session_id)
        session_object = {
            'date_time': datetime.now(),
            'session_id': session_id,
            'adaptivity_instance': Adaptation(),
            'last_operation': None,
            'task_count': 0
        }

        self.sessions.append(session_object)

    def garbage_collect_session_list(self):
        accepted_time = datetime.now() + timedelta(hours = -1)
        results = [idx for idx, element in enumerate(self.sessions) if element['date_time'] < accepted_time]

        print("remove %s old session IDs" % len(results))
        for index in results:
            del self.sessions[index]

        return len(results)

    def write_log(self, session_id, x = 'NA', operation = 'NA', y = 'NA', result = 'NA', estimated_difficulty = 'NA', individual_factor = 'NA', time_offset = 'NA', correct_response = 'NA', response_time = 'NA', in_time = 'NA'):
        fh = open(self.log_filename, "a")
        fh.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (
            session_id,
            datetime.now(),
            x,
            operation,
            y,
            result,
            estimated_difficulty,
            individual_factor,
            time_offset,
            correct_response,
            response_time,
            in_time
        ))
        fh.close()


if __name__ == "__main__":
    estimator_system = DifficultyEstimationSystem()
    seconds = estimator_system.estimate_time_needed_for_task(27, 7, "mal")
    print("Main Unit")
    print("testrun: for the mental calculation of 27 * 7 we estimate to need %s s" % seconds)
