//
//  StateMachineExtension.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 14.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation
import ARKit



extension ViewController {
    
    func nextState() {
        // decide which state to enter next
        switch currentState {
        case 0:
            currentState = 1
            setInitialState()
            
        case 1:
            currentState = 2
            setShowConditionState()

        case 2:
            currentState = 3
            setTrialState()

        case 3:
            stopFaceTracking()
            if currentConditionNumber >= (conditionOrder.count - 1) {
                currentState = 4
                setFinalState()
                
            } else {
                currentConditionNumber += 1
                currentState = 2
                setShowConditionState()
            }

        default:
            currentState = 1
            NSLog("undefined state!")
        }
    }
    
    // state 1
    func setInitialState() {
        startStudyViewContainer.isHidden = false
        startConditionViewContainer.isHidden = true
        interactionViewContainer.isHidden = true
        statsViewContainer.isHidden = true
        finalStateViewContainer.isHidden = true
        
        // prepare condition order
        conditionOrder.shuffle()
        conditionOrder = [0] + conditionOrder
        logInteraction(action: "define condition order: \(conditionOrder[0])-\(conditionOrder[1])-\(conditionOrder[2])-\(conditionOrder[3])")
    }
    
    // state 2
    func setShowConditionState() {
        currentCondition = conditionOrder[currentConditionNumber]
        if currentCondition == 0 {
            conditionLabel.text = "Training"
        } else {
            conditionLabel.text = String(currentCondition)
        }
        
        disableStartTrialsButton()
        
        startStudyViewContainer.isHidden = true
        startConditionViewContainer.isHidden = false
        interactionViewContainer.isHidden = true
        statsViewContainer.isHidden = true
        finalStateViewContainer.isHidden = true
        
        conditionIsRunning = true
        current_session = ""
        logInteraction(action: "show current condition ID: \(currentCondition)")
        
        Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(enableStartTrialsButton), userInfo: nil, repeats: false)
    }
    
    // state 3
    func setTrialState() {
        startStudyViewContainer.isHidden = true
        startConditionViewContainer.isHidden = true
        interactionViewContainer.isHidden = true
        statsViewContainer.isHidden = true
        finalStateViewContainer.isHidden = true
        
        resetGame()
        startGameButton.isHidden = true
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startTrials), userInfo: nil, repeats: false)
        logInteraction(action: "start trials of condition \(currentCondition)")
        
        if ARFaceTrackingConfiguration.isSupported {
            startFaceTracking()
        }
    }
    
    // state 4
    func setFinalState() {
        startStudyViewContainer.isHidden = true
        startConditionViewContainer.isHidden = true
        interactionViewContainer.isHidden = true
        statsViewContainer.isHidden = true
        finalStateViewContainer.isHidden = false
        logInteraction(action: "completed data aquisition")
        
        // show reward text
        let rewardNotice = createRewardString()
        compensationNoticeLabel.text = rewardNotice
        logInteraction(action: rewardNotice)
    }
    
}
