//
//  Structs.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 14.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation



struct Task {
    let x: Int
    let y: Int
    let result: Int
    let operation: String
    var time: Double
    let performance: Double
    let offset: Double
}

struct Stats {
    let score: Double
    let correctness: Double
    let performanceIndex: Double
    let inTimeResponses: Double
    let timeLeft: Double
}

struct LogEntry {
    var id: String
    var timestamp = Date()
    var action: String
    var value: Double?
    var timeAfterTaskOnset: Double?
    var condition: Int?
    var trialNo: Int?
    var didRespond: Bool?
    var correctResponse: Bool?
    var taskStartTime: Date?
    var taskTimeGiven: TimeInterval?
    var responseTime: TimeInterval?
    var taskOperation: String?
    var taskX: Int?
    var taskY: Int?
    var taskResult: Int?
    var taskResponse: Int?
    
    func toString() -> String {
        let ts = rsDateFormatter.string(from: timestamp)
        
        var val = ""
        if let v = value {
            val = String(format: "%.3f", v)
        }
        
        var timeAfterOnset = ""
        if let timeAfterOn = timeAfterTaskOnset {
            timeAfterOnset = String(timeAfterOn)
        }
        
        var cond = ""
        if let c = condition {
            cond = String(c)
        }
        
        var trial = ""
        if let t = trialNo {
            trial = String(t)
        }
        
        var didResp = ""
        if let resp = didRespond {
            didResp = resp ? "true" : "false"
        }
        
        var correctResp = ""
        if let resp = correctResponse {
            correctResp = resp ? "true" : "false"
        }
        
        var startTs = ""
        if let startTime = taskStartTime {
            startTs = rsDateFormatter.string(from: startTime)
        }
        
        var taskTime = ""
        if let t = taskTimeGiven {
            taskTime = String(t)
        }
        
        var respTime = ""
        if let t = responseTime {
            respTime = String(t)
        }
        
        var x = ""
        if let t = taskX {
            x = String(t)
        }
        
        var y = ""
        if let t = taskY {
            y = String(t)
        }
        
        var res = ""
        if let t = taskResult {
            res = String(t)
        }
        
        var resp = ""
        if let t = taskResponse {
            resp = String(t)
        }
        
        return "\(id),\(ts),\(action),\(val),\(timeAfterOnset),\(cond),\(trial),\(didResp),\(correctResp),\(startTs),\(taskTime),\(respTime),\(taskOperation ?? ""),\(x),\(y),\(res),\(resp)"
    }
}
