//
//  GameLogicExtension.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 14.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation
import UIKit
import Alamofire



extension ViewController {
    
    func resetGame() {
        countdown?.invalidate()
        taskStartDate = nil
        submitBlocked = true
        startGameButton.isHidden = false
        timeLeftProgressBar.isHidden = true
        resultInputField.isHidden = true
        resultInputField.resignFirstResponder()
        resultInputField.isEnabled = false
        
        showStats(visible: false)
        interactionViewContainer.isHidden = true
        
        timeLeftLabel.text = ""
        timeLeftProgressBar.progress = 0
        taskTextField.text = "Ready?"
        resultInputField.text = ""
        feedbackLabel.text = ""
    }
    
    @objc func startNextTask() {
        var operand = ""
        switch current_task!.operation {
        case "addition":
            operand = "+"
        case "subtraction":
            operand = "-"
        case "multiplication":
            operand = "x"
        case "division":
            operand = "/"
        default:
            operand = current_task!.operation
        }
        
        taskTextField.text = "\(current_task!.x) \(operand) \(current_task!.y)"
        resultInputField.text = ""
        resultInputField.isEnabled = true
        interactionViewContainer.isHidden = false
        submitBlocked = false
        currentTrialNo += 1
    
        // update time left label
        timeLeft = current_task!.time
        updateTimeLeftLabel()
        timeLeftLabel.isHidden = false
        
        // start countdown
        countdown = Timer.scheduledTimer(timeInterval: 0.075, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
        taskStartDate = Date()
    }
    
    func submitResult(timeout: Bool) {
        guard !submitBlocked else { return }
        
        countdown?.invalidate()
        taskStartDate = nil
        submitBlocked = true
        resultInputField.isEnabled = false
        timeLeftLabel.isHidden = true
        interactionViewContainer.isHidden = true

        if timeout {
            timeLeft = 0
        }
        
        let response = Int(resultInputField.text!)
        let correct = ((response ?? -1) == current_task!.result)
        let response_time = current_task!.time - timeLeft
        let was_in_time = !timeout || correct
        taskCountPerOperation[current_task!.operation]! += 1

        taskTextField.text = ""
        resultInputField.text = ""
        showFeedback(correctResponse: correct, result: current_task!.result)
        
        // log task
        logTaskResponse(didRespond: was_in_time, correctResponse: correct, responseTime: response_time, response: response)
        if correct {
            totalCorrectTaskResponseNumber += 1
        }
        
        let params : Parameters = [
            "correct_response": correct ? 1 : 0,
            "response_time": response_time,
            "in_time": was_in_time ? 1 : 0,
            "operation": current_task!.operation,
            "session_id": current_session
        ]
        
        // request next task and upload stats of current task
        if conditionIsRunning {
            requestTask(parameter: params)
        } else {
            // jump to next condition if time is over
            nextState()
        }
    }
    
    func showStats(visible: Bool) {
        statsViewContainer.isHidden = !visible
    }
    
    func showFeedback(correctResponse: Bool, result: Int) {
        if correctResponse {
            feedbackLabel.text = "correct!"
            feedbackLabel.textColor = keyColorBlue
        } else {
            feedbackLabel.text = String(current_task!.result)
            feedbackLabel.textColor = keyColorRed
        }
        
        feedbackLabel.alpha = 1
        UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseIn, animations: { () -> Void in
            self.feedbackLabel.alpha = 0
        },
        completion: nil)
    }
    
    @objc func updateCounter() {
        guard let startDate = taskStartDate else { return }
        
        let interval = Date().timeIntervalSince(startDate)
        timeLeft = current_task!.time - interval
        
        if timeLeft > 0 {
            updateTimeLeftLabel()

            if timeLeft > 3 {
                timeLeftProgressBar.tintColor = keyColorBlue
            } else {
                timeLeftProgressBar.tintColor = keyColorRed
            }
            
            let progressValue = timeLeft / current_task!.time
            timeLeftProgressBar.progress = Float(progressValue)

        } else {
            countdown?.invalidate()
            taskStartDate = nil
            submitResult(timeout: true)
        }
    }
    
    func updateTimeLeftLabel() {
        timeLeftLabel.text = "\(String(format: "%.1f", timeLeft)) sec"
    }
    
//    // auto-submit result as soon as it's correct
//    @objc func resultTextFieldDidChange(_ textField: UITextField) {
//        if (Int(resultInputField.text!) == current_task!.result) {
//            submitResult(timeout: false)
//        }
//    }
    
}
