//
//  ViewController.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 12.01.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import UIKit
import SceneKit
import ARKit



class ViewController: UIViewController, ARSCNViewDelegate {

    // start study & VP code outlets
    @IBOutlet var startStudyViewContainer: UIView!
    @IBOutlet var subjectNumberTextField: UITextField!
    @IBOutlet var startStudyButton: UIButton!
    
    // start condition outlets
    @IBOutlet var startConditionViewContainer: UIView!
    @IBOutlet var conditionLabel: UILabel!
    @IBOutlet var startConditionButton: UIButton!
    
    // task outlets
    @IBOutlet var timeLeftLabel: UILabel!
    @IBOutlet var timeLeftProgressBar: UIProgressView!
    @IBOutlet var taskTextField: UILabel!
    @IBOutlet var resultInputField: UITextField!
    @IBOutlet var startGameButton: UIButton!
    @IBOutlet var feedbackLabel: UILabel!
    
    // interaction outlets
    @IBOutlet var interactionViewContainer: UIView!
    @IBOutlet var submitNextButton: UIView!
    @IBOutlet var number1: UIButton!
    @IBOutlet var number2: UIButton!
    @IBOutlet var number3: UIButton!
    @IBOutlet var number4: UIButton!
    @IBOutlet var number5: UIButton!
    @IBOutlet var number6: UIButton!
    @IBOutlet var number7: UIButton!
    @IBOutlet var number8: UIButton!
    @IBOutlet var number9: UIButton!
    @IBOutlet var number0: UIButton!
    @IBOutlet var pauseButton: UIButton!
    @IBOutlet var resetButton: UIButton!
    
    // stats outlets
    @IBOutlet var statsViewContainer: UIView!
    @IBOutlet var statsCorrectLabel: UILabel!
    @IBOutlet var statsInTimeLabel: UILabel!
    @IBOutlet var statsPerformanceLabel: UILabel!
    @IBOutlet var statsTimeLeftLabel: UILabel!
    @IBOutlet var taskCountLabel: UILabel!
    
    // final state outlets
    @IBOutlet var finalStateViewContainer: UIView!
    @IBOutlet var compensationNoticeLabel: UILabel!
    @IBOutlet var generateDatasetButton: UIButton!
    
    // ARKit
    @IBOutlet var sceneView: ARSCNView!
    
    
    
    let keyColorRed = UIColor.init(cgColor: CGColor(srgbRed: 219 / 255, green: 84 / 255, blue: 97 / 255, alpha: 1.0))
    let keyColorBlue = UIColor.init(cgColor: CGColor(srgbRed: 56 / 255, green: 145 / 255, blue: 166 / 255, alpha: 1.0))
    let keyColorDark = UIColor.init(cgColor: CGColor(srgbRed: 76 / 255, green: 91 / 255, blue: 92 / 255, alpha: 1.0))
    
    // study parameters
    let delayBetweenTrials = 0.5
    let timeForTraining = 5.0
    let timePerCondition = 60.0
    let fixedTimeConditionTrialTime = 7.5
    let maxBonus = 6.0
    
    var current_session = ""
    var current_task: Task?
    var current_stats: Stats?
    var timeLeft = 0.0
    var countdown: Timer?
    var taskStartDate: Date?
    var submitBlocked = true
    var taskCount = 0
    var taskCountPerOperation = [
        "addition": 0,
        "subtraction": 0,
        "division": 0,
        "multiplication": 0,
    ]

    // eye tracking
    var blinkStartDate = Date()
    var currentlyBlinking = false
    var lookAwayStartDate = Date()
    var currentlyLookingAway = false
    
    var currentState = 0
    var currentVPcode = ""
    var currentConditionNumber = 0
    var currentCondition = 0
    var conditionOrder = [1, 2, 3]
    var conditionTimer: Timer?
    var conditionIsRunning = false
    var logList: [LogEntry] = []
    
    // stats
    let estimatedMaxNumberOfTrials = 500.0
    var currentTrialNo = 0
    var totalCorrectTaskResponseNumber = 0.0
    
        
    
    // MARK: - UI & class methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        rsDateFormatter.timeZone = TimeZone.current
        rsDateFormatter.dateFormat = "yy-MM-dd_HH-mm-ss"
        
        // setup visibles
        interactionViewContainer.backgroundColor = .none
        submitNextButton.layer.cornerRadius = 5
        
        // setup ARKit
        sceneView.delegate = self
     
//        guard ARFaceTrackingConfiguration.isSupported else {
//            fatalError("Face tracking is not supported on this device")
//        }
        
        // enter in first state
        nextState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    
    // MARK: - Trial Interaction
    
    @IBAction func startButtonTapped(_ sender: Any) {
        startGameButton.isHidden = true
        resultInputField.isHidden = false
        timeLeftProgressBar.isHidden = false
        showStats(visible: false)
        
        var params: [String: Any] = [:]
        if !current_session.isEmpty {
            params = ["session_id": current_session]
        }
        requestTask(parameter: params, withDelay: false)
    }
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        submitResult(timeout: false)
    }
    
    @IBAction func pauseButtonTapped(_ sender: Any) {
        resetGame()
        showStats(visible: true)
    }
    
    @IBAction func resetButtonTapped(_ sender: Any) {
        resultInputField.text = ""
    }
    
    @IBAction func number1tapped(_ sender: Any) {
        resultInputField.text?.append("1")
    }
    
    @IBAction func number2tapped(_ sender: Any) {
        resultInputField.text?.append("2")
    }
    
    @IBAction func number3tapped(_ sender: Any) {
        resultInputField.text?.append("3")
    }
    
    @IBAction func number4tapped(_ sender: Any) {
        resultInputField.text?.append("4")
    }
    
    @IBAction func number5tapped(_ sender: Any) {
        resultInputField.text?.append("5")
    }
    
    @IBAction func number6tapped(_ sender: Any) {
        resultInputField.text?.append("6")
    }
    
    @IBAction func number7tapped(_ sender: Any) {
        resultInputField.text?.append("7")
    }
    
    @IBAction func number8tapped(_ sender: Any) {
        resultInputField.text?.append("8")
    }
    
    @IBAction func number9tapped(_ sender: Any) {
        resultInputField.text?.append("9")
    }
    
    @IBAction func number0tapped(_ sender: Any) {
        resultInputField.text?.append("0")
    }
    
    
    
    // MARK: - Start Study
    
    @IBAction func startStudyButtonTapped(_ sender: Any) {
        guard !subjectNumberTextField.text!.isEmpty else { return }
        
        subjectNumberTextField.resignFirstResponder()
        currentVPcode = subjectNumberTextField.text!
        logInteraction(action: "start study button tapped")
        
        nextState()
    }
    
    
    
    // MARK: - Start Study
    @IBAction func startConditionButtonTapped(_ sender: Any) {
        logInteraction(action: "start condition button tapped")
        nextState()
    }
    
    
    
    // MARK: - Final State
    @IBAction func generateDatesetButtonTapped(_ sender: Any) {
        logInteraction(action: "generate data button tapped")
        let (csvFilename, csvContent) = createLogFile()
        
        // write file & open share view
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(csvFilename)
            
            do {
                try csvContent.write(to: fileURL, atomically: false, encoding: .utf8)
                
                let objectsToShare = [fileURL]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                self.present(activityVC, animated: true, completion: nil)
            }
            catch {
                print("error while writing file")
            }
        }
    }
    
}
