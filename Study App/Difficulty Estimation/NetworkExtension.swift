//
//  NetworkExtension.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 14.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



let backend_address = "https://YOURSERVER"



extension ViewController {
    
    func requestTask(parameter: Dictionary<String, Any>?, withDelay: Bool = true) {
        Alamofire.request(
            "\(backend_address)/next_task",
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.httpBody
            ).validate().responseJSON { response in
                   switch response.result {
                   case .success:
                       print("Validation Successful)")

                       if let json = response.data {
                        do{
                            let data = try JSON(data: json)
                            self.current_session = data["session_id"].string!
                            
                            let x = data["first_number"].int
                            let y = data["second_number"].int
                            let result = data["result"].int
                            let operation = data["operation"].string
                            
                            // manipulate time per task depending on the current condition
                            var time = data["difficulty"].double
                            
                            if self.currentConditionIsTraining() {
                                time = self.fixedTimeConditionTrialTime * 2
                            } else if self.currentConditionIsFixedTime() {
                                time = self.fixedTimeConditionTrialTime
                            }
                            
                            let performance = data["individual_factor"].double
                            let offset = data["time_offset"].double
                            self.current_task = Task(x: x!, y: y!, result: result!, operation: operation!, time: time!, performance: performance!, offset: offset!)
                            
                            let score = data["overall_score"].double
                            let correctness = data["correctness"].double
                            let overallPerformance = data["performance_index"].double
                            let inTime = data["in_time_responses"].double
                            let timeLeft = data["average_time_left"].double
                            self.current_stats = Stats(score: score!, correctness: correctness!, performanceIndex: overallPerformance!, inTimeResponses: inTime!, timeLeft: timeLeft!)
                            
                            // start next task with delay
                            if withDelay {
                                Timer.scheduledTimer(timeInterval: self.delayBetweenTrials, target: self, selector: #selector(self.startNextTask), userInfo: nil, repeats: false)
                            } else {
                                self.startNextTask()
                            }

                        } catch {
                           print("JSON Error")
                        }

                       }
                   case .failure(let error):
                       print(error)
                   }
               }
    }
    
}
