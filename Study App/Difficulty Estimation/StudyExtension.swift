//
//  StudyExtension.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 17.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation
import UIKit



extension ViewController {
    
    @objc func setConditionTimeUp() {
        conditionTimer?.invalidate()
        conditionIsRunning = false
    }

    @objc func startTrials() {
        startGameButton.isHidden = true
        resultInputField.isHidden = false
        timeLeftProgressBar.isHidden = false
        showStats(visible: false)
        
        var params: [String: Any] = [:]
        if !current_session.isEmpty {
            params = ["session_id": current_session]
        }
        requestTask(parameter: params, withDelay: false)
        
        // set timer for
        var timerInterval = timePerCondition
        if conditionOrder[currentConditionNumber] == 0 {
            timerInterval = timeForTraining
        }
        
        conditionTimer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(setConditionTimeUp), userInfo: nil, repeats: false)
    }
    
    func disableStartTrialsButton() {
        startConditionButton.isEnabled = false
        startConditionButton.backgroundColor = .lightGray
        startConditionButton.setTitle("wait...", for: .normal)
    }
    
    @objc func enableStartTrialsButton() {
        startConditionButton.isEnabled = true
        startConditionButton.backgroundColor = keyColorRed
        startConditionButton.setTitle("Tap to start!", for: .normal)
    }
    
    func currentConditionIsFixedTime() -> Bool {
        return conditionOrder[currentConditionNumber] == 1
    }
    
    func currentConditionIsTraining() -> Bool {
        return conditionOrder[currentConditionNumber] == 0
    }
    
    func createRewardString() -> String {
        var taskCountRatio = Double(round(Double(currentTrialNo) / estimatedMaxNumberOfTrials * 1000) / 1000)
        if taskCountRatio > 1.0 {
            taskCountRatio = 1.0
        }

        let correct_ratio = Double(round(totalCorrectTaskResponseNumber / Double(currentTrialNo) * 1000) / 1000)
        let bonus = Double(ceil(maxBonus * taskCountRatio * correct_ratio * 10) / 10)

        return "\(correct_ratio * 100.0)% of your responses were correct and you answered \(taskCountRatio * 100.0)% of the tasks. Thus, your compensation is 12€ + \(bonus)€ = \(12 + bonus)€"
    }
    
    func logInteraction(action: String) {
        let logEntry = LogEntry(
            id: currentVPcode,
            action: action
        )
        
        logList.append(logEntry)
    }
    
    func logEvent(event: String, value: Double) {
        var timeAfterTaskOnset = -1.0
        if let taskStart = taskStartDate {
            timeAfterTaskOnset = Date().timeIntervalSince(taskStart)
        }
        
        let logEntry = LogEntry(
            id: currentVPcode,
            action: event,
            value: value,
            timeAfterTaskOnset: timeAfterTaskOnset,
            condition: conditionOrder[currentConditionNumber],
            trialNo: currentTrialNo
        )
        
        logList.append(logEntry)
        print("logged event '\(event)' (\(value), after \(timeAfterTaskOnset)s")
    }
    
    func logTaskResponse(didRespond: Bool, correctResponse: Bool, responseTime: TimeInterval, response: Int?) {
        let logEntry = LogEntry(
            id: currentVPcode,
            timestamp: Date(),
            action: "taskResponse",
            condition: conditionOrder[currentConditionNumber],
            trialNo: currentTrialNo,
            didRespond: didRespond,
            correctResponse: correctResponse,
            taskStartTime: taskStartDate,
            taskTimeGiven: current_task!.time,
            responseTime: responseTime,
            taskOperation: current_task!.operation,
            taskX: current_task!.x,
            taskY: current_task!.y,
            taskResult: current_task!.result,
            taskResponse: response
        )
        
        logList.append(logEntry)
    }
    
    func createLogFile() -> (String, String) {
        let csvHead = "id,timestamp,action,value,timeAfterTaskOnset,condition,trialNo,didRespond,correctResponse,taskStartTime,taskTimeGiven,responseTime,taskOperation,taskX,taskY,taskResult,taskResponse\n"
        let logRows = logList.map { (entry) -> String in entry.toString() }
        let csvContent = csvHead + logRows.joined(separator: "\n")
        
        let dateTime = rsDateFormatter.string(for: Date())!
        let csvFilename = "\(currentVPcode)_\(dateTime)_mental_calc_data.csv"
        
        return (csvFilename, csvContent)
    }
    
}
