//
//  ARKitExtension.swift
//  Difficulty Estimation
//
//  Created by Robert Greinacher on 18.10.20.
//  Copyright © 2020 Robert Greinacher. All rights reserved.
//

import Foundation
import ARKit



extension ViewController {
    
    func stopFaceTracking() {
        sceneView.session.pause()
    }
    
    func startFaceTracking() {
        let configuration = ARFaceTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    
    
    // MARK: - Analysis
    
    func faceAnalysis(anchor: ARFaceAnchor, node: SCNNode) {

        // blink detection & logging
        let blinkLeft = anchor.blendShapes[.eyeBlinkLeft]?.decimalValue ?? 0.0
        let blinkRight = anchor.blendShapes[.eyeBlinkRight]?.decimalValue ?? 0.0
        
        if blinkLeft >= 0.8 && blinkRight >= 0.8 {
            if !currentlyBlinking {
                currentlyBlinking = true
                blinkStartDate = Date()
            }

        } else if currentlyBlinking {
            currentlyBlinking = false
            let blinkDuration = Date().timeIntervalSince(blinkStartDate)
            logEvent(event: "blinkDuration", value: blinkDuration)
        }
        
        // look off screen detection & logging
        if let lookAtScreen = self.userLooksAtScreen(faceAnchor: anchor, node: node) {
            
            if !lookAtScreen {
                if !currentlyLookingAway && !currentlyBlinking {
                    currentlyLookingAway = true
                    lookAwayStartDate = Date()
                }
                
            } else if currentlyLookingAway {
                currentlyLookingAway = false
                let lookAwayDuration = Date().timeIntervalSince(lookAwayStartDate)
                if lookAwayDuration > 0.2 {
                    logEvent(event: "lookAwayDuration", value: lookAwayDuration)
                }
            }
        }
    }
    
    func userLooksAtScreen(faceAnchor: ARFaceAnchor, node: SCNNode) -> Bool? {
        guard let pov = sceneView.pointOfView else { return nil }
        
        let faceOrientation: simd_quatf = simd_quatf(node.simdWorldTransform)
        let cameraOrientation: simd_quatf = simd_quatf(pov.simdWorldTransform)
        let deltaOrientation: simd_quatf = faceOrientation.inverse * cameraOrientation
        let angles = deltaOrientation.axis * deltaOrientation.angle

        // face angles, relative to camera
        let pitch = angles.x
        let yaw = angles.y
    
        // eye position
        let eyeX = faceAnchor.lookAtPoint.x
        let eyeY = faceAnchor.lookAtPoint.y

        let differenceX = yaw - eyeX
        let thresholdX : Float = 0.095
    
        let differenceY = (-2.5 * eyeY) - pitch
        let thresholdY : Float = 0.32
        
        return abs(differenceX) < thresholdX && abs(differenceY) < thresholdY
    }
    
    
    
    // MARK: - ARSCNViewDelegate
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let faceMesh = ARSCNFaceGeometry(device: sceneView.device!)
        let node = SCNNode(geometry: faceMesh)
//        node.geometry?.firstMaterial?.fillMode = .lines
        return node
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        if let faceAnchor = anchor as? ARFaceAnchor, let faceGeometry = node.geometry as? ARSCNFaceGeometry {
            faceGeometry.update(from: faceAnchor.geometry)
            faceAnalysis(anchor: faceAnchor, node: node)
        }
    }
    
}
