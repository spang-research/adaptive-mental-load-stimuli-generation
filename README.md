# Welcome!

This is the supplementary software repository for the paper **Because You’re Special: Adaptive Mental Load Stimuli Generation and Its Effects on Response Behavior** by Robert Spang and Kerstin Pieper, 2021; TU Berlin. Currently, the manuscript is under review. A link to the paper as well as citation information will be added as soon as it’s available.

This repository contains two major components:

-	The Python driven difficulty estimation system can be found in the “Stimulus Generator” folder. It contains everything you need to spin up the adaptive task generation as described in the paper.
-	The iOS app used in to record samples during user studies is to be found in the “Study App” folder. In there, you’ll find an XCode project to build the app as we used it in our experiments. Note: please change the “backend_address” variable in the “NetworkExtension.swift” file to match your server running the difficulty estimation system.

If you have any questions, please reach out to Robert Spang (research *at*  robert-spang.de).

Disclaimer: This is research code and not production ready. Consider it as a proof of concept / prototype.
